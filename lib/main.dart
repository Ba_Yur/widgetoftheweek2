import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: HomePage(),
    );
  }
}

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Widget of the week #2'),
      ),
      body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              child: Stack(
                alignment: Alignment.center,
                children: <Widget>[
                  Container(
                    height: 200,
                    width: 200,
                    color: Colors.green,
                  ),
                  Container(
                    height: 150,
                    width: 150,
                    color: Colors.red,
                  ),
                  Container(
                    height: 100,
                    width: 100,
                    color: Colors.grey,
                  ),
                  Positioned(
                    bottom: 0,
                    child: Container(
                      child: Icon(Icons.accessibility),
                    ),
                  ),
                  Positioned(
                      top: -50,
                      left: -50,
                      child: Container(
                        width: 150,
                        height: 150,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: Colors.blue,
                        ),
                      ))
                ],
              ),
            ),
            Divider(
              height: 100,
              thickness: 10,
              indent: 100,
              color: Colors.greenAccent,
            ),
            Row(
              children: [
                Container(
                  height: 100,
                  width: 100,
                  color: Colors.blue,
                ),
                Spacer(),
                Container(
                  height: 100,
                  width: 100,
                  color: Colors.blue,
                ),
                Spacer(
                  flex: 3,
                ),
                Container(
                  height: 100,
                  width: 100,
                  color: Colors.blue,
                ),
              ],
            ),
            Divider(
              height: 100,
              thickness: 10,
              endIndent: 100,
              color: Colors.greenAccent,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.adb_sharp),
      ),
      bottomNavigationBar: BottomAppBar(
        child: Icon(Icons.accessibility_outlined),
        color: Colors.grey,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
      // bottomNavigationBar: BottomNavigationBar(),
    );
  }
}
